const { Router } = require("express");
const UserService = require("../services/userService");
const { createUserValid, updateUserValid } = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user

router.get("/", (req, res, next) => {
  const users = UserService.searchAll();
  if (users) {
    return res.json(users);
  }
  return res.status(404).json({ error: true, message: "Users not found" });
});

router.get("/:id", (req, res, next) => {
  const userId = req.params.id;
  const user = UserService.search({ id: userId });
  if (user) {
    return res.json(user);
  }
  return res.status(404).json({ error: true, message: "User not found" });
});

router.post("/", createUserValid, (req, res, next) => {
  const users = UserService.createUser(req.body);
  return res.status(200).json(users);
});

router.put("/:id", updateUserValid, (req, res, next) => {
  const data = UserService.updateUser(req.params.id, req.body);
  return res.status(200).json(data);
});

router.delete("/:id", (req, res, next) => {
  const userId = req.params.id;
  const user = UserService.remove({ id: userId });
  return res.status(200).json(user);
});

module.exports = router;
