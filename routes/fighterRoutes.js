const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const { createFighterValid, updateFighterValid } = require("../middlewares/fighter.validation.middleware");
const { fighter } = require("../models/fighter");

const router = Router();

// TODO: Implement route controllers for fighter

router.get("/", (req, res, next) => {
  const fighters = FighterService.searchAll();
  if (fighters) {
    return res.status(200).json(fighters);
  }
  return res.status(404).json({ error: true, message: "Figher not found" });
});

router.get("/:id", (req, res, next) => {
  const fighterId = req.params.id;
  const fighter = FighterService.search({ id: fighterId });
  if (fighter) {
    return res.status(200).json(fighter);
  }
  return res.status(404).json({ error: true, message: "Figher not found" });
});

router.post("/", createFighterValid, (req, res, next) => {
  const fighter = FighterService.createFighter(req.body);
  return res.status(200).json(fighter);
});

router.put("/:id", updateFighterValid, (req, res, next) => {
  const data = FighterService.updateFighter(req.params.id, req.body);
  return res.status(200).json(data);
});

router.delete("/:id", (req, res, next) => {
  const fighterId = req.params.id;
  const fighter = FighterService.remove({ id: fighterId });
  return res.status(200).json(fighter);
});

module.exports = router;
