const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user

  searchAll() {
    const items = UserRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  createUser(data) {
    const user = UserRepository.create(data);
    if (!user) {
      return null;
    }
    return user;
  }

  updateUser(id, data) {
    if (!id || !data) {
      return null;
    }
    const item = UserRepository.update(id, data);
    return item;
  }

  remove(search) {
    const item = UserRepository.delete(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
