const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters

  searchAll() {
    const items = FighterRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  createFighter(data) {
    const fighter = FighterRepository.create(data);
    if (!fighter) {
      return null;
    }
    return fighter;
  }

  updateFighter(id, data) {
    if (!id || !data) {
      return null;
    }
    const item = FighterRepository.update(id, data);
    return item;
  }

  remove(search) {
    const item = FighterRepository.delete(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();
