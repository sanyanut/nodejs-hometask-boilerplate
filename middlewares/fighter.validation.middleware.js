const { fighter } = require("../models/fighter");
const Joi = require("joi");

const createFighterValid = (req, res, next) => {
  const { body } = req;

  const createFighterScheme = Joi.object().keys({
    name: Joi.string()
      .regex(/^[a-zA-Z][a-zA-Z0-9 ]+$/)
      .required(),
    health: Joi.number().required(),
    power: Joi.number().min(1).max(100).required(),
    defense: Joi.number().min(1).max(10).required(),
  });

  const result = createFighterScheme.validate(body);
  const { error } = result;
  const valid = error == null;

  if (!valid) {
    return res.status(400).json({ error: true, message: "Invalid POST request body" });
  }

  return next();
};

const updateFighterValid = (req, res, next) => {
  const { body } = req;

  const updateFighterScheme = Joi.object().keys({
    name: Joi.string().regex(/^[a-zA-Z][a-zA-Z0-9 ]+$/),
    health: Joi.number(),
    power: Joi.number().min(1).max(100),
    defense: Joi.number().min(1).max(10),
  });

  const result = updateFighterScheme.validate(body);
  const { error } = result;
  const valid = error == null;

  if (!valid) {
    return res.status(400).json({ error: true, message: "Invalid POST request body" });
  }

  return next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
