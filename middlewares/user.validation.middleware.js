const { user } = require("../models/user");
//const { check, validationResult, body } = require("express-validator");
const Joi = require("joi");

const userScheme = Object.assign({}, user);
delete userScheme.id;
userScheme.firstName = (value) => /^[a-zA-Z][a-zA-Z0-9 ]+$/.test(value);
userScheme.lastName = (value) => /^[a-zA-Z][a-zA-Z0-9 ]+$/.test(value);
userScheme.email = (value) => /^[a-zA-Z][-_.a-zA-Z0-9]{5,29}@g(oogle)?mail.com$/.test(value);
userScheme.phoneNumber = (value) => /^(^\+380|^251|^0)?9\d{8}$/.test(value);
userScheme.password = (value) => value && value.length > 2;

//EXPERIMENTAL Non library req.body validation

const createUserValid = (req, res, next) => {
  const userBody = req.body;
  for (const userSchemeProp in userScheme) {
    const validate = userScheme[userSchemeProp](userBody[userSchemeProp]);
    if (
      !userBody.hasOwnProperty(userSchemeProp) ||
      Object.keys(userBody).length !== Object.keys(userScheme).length ||
      !validate
    ) {
      return res.status(400).json({ error: true, message: "Invalid POST request body" });
    }
  }
  return next();
};

const updateUserValid = (req, res, next) => {
  const { body } = req;
  const updateUserScheme = Joi.object().keys({
    firstName: Joi.string().regex(/^[a-zA-Z][a-zA-Z0-9 ]+$/),
    lastName: Joi.string().regex(/^[a-zA-Z][a-zA-Z0-9 ]+$/),
    email: Joi.string().regex(/^[a-zA-Z][-_.a-zA-Z0-9]{5,29}@g(oogle)?mail.com$/),
    phoneNumber: Joi.string().regex(/^(^\+380|^251|^0)?9\d{8}$/),
    password: Joi.string().min(3),
  });
  const result = updateUserScheme.validate(body);
  const { error } = result;
  const valid = error == null;

  if (!valid) {
    return res.status(400).json({ error: true, message: "Invalid POST request body" });
  }
  return next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
